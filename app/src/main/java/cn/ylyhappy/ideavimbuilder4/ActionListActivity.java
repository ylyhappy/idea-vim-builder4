package cn.ylyhappy.ideavimbuilder4;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.SearchView;
import android.widget.Switch;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.ylyhappy.ideavimbuilder4.constant.Constant;
import cn.ylyhappy.ideavimbuilder4.db.DbHelper;
import cn.ylyhappy.ideavimbuilder4.dialog.DialogFactory;
import cn.ylyhappy.ideavimbuilder4.model.IdeaVimAction;
import cn.ylyhappy.ideavimbuilder4.model.IdeaVimConfig;
import cn.ylyhappy.ideavimbuilder4.recyclerlist.ActionListActivityRecyclerListAdapter;

public class ActionListActivity extends BaseActivity {

    private SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_action_list);
        initInsets();

        Intent intent = getIntent();
        // 这个api需要min33 intent.getParcelableExtra("ConfigData", IdeaVimConfig.class);
        IdeaVimConfig configData = intent.getParcelableExtra(Constant.INTENT_IDEA_VIM_CONFIG_DATA);

        Switch sw = findViewById(R.id.activity_action_list_switch_zh_en);
        // sw.onChange

        RecyclerView recyclerView = findViewById(R.id.activity_action_list_recyclerview1);
        ArrayList<IdeaVimAction> list = (ArrayList<IdeaVimAction>) DbHelper.getInstance().getIdeaVimActions();
        ActionListActivityRecyclerListAdapter adapter = new ActionListActivityRecyclerListAdapter(list, (c, e, p) -> {
            DialogFactory.buildActionActivityModalFormDialog(this,c, e, p).show();
        });
        recyclerView.setAdapter(adapter);
        adapter.setConifgId(configData.getId());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        sv = findViewById(R.id.activity_action_list_search_view1);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (sw.isChecked()) {
                    adapter.setFilter(e -> e.getZh().contains(newText));
                } else {
                    adapter.setFilter(e -> e.getEn().toLowerCase().contains(newText.toLowerCase()));
                }
                return false;
            }
        });
    }
}
