package cn.ylyhappy.ideavimbuilder4;


import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;

import cn.ylyhappy.ideavimbuilder4.constant.Constant;
import cn.ylyhappy.ideavimbuilder4.db.DbHelper;
import cn.ylyhappy.ideavimbuilder4.model.IdeaVimConfig;

public class ConfigPreviewActivity extends BaseActivity {
    private TextView tv1;
    private TextView tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_preview_config);
        initInsets();

        Intent intent = getIntent();
        // 这个api需要min33 intent.getParcelableExtra("ConfigData", IdeaVimConfig.class);
        IdeaVimConfig configData = intent.getParcelableExtra(Constant.INTENT_IDEA_VIM_CONFIG_DATA);

        // title text
        tv1 = findViewById(R.id.activity_preview_textview1);
        // preview text
        tv2 = findViewById(R.id.activity_preview_textview2);

        tv1.setText(configData.getName());
        String preivew = DbHelper.getInstance().getPreviewIdeaVimConfig(configData.getId(), false);
        tv2.setText(preivew);

    }
}
