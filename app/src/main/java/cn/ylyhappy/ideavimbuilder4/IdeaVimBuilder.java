package cn.ylyhappy.ideavimbuilder4;

import android.app.Application;
import android.content.Context;

public class IdeaVimBuilder extends Application {
    private static Context mContext;


    public static Context getAppContext() {
        return IdeaVimBuilder.mContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }


}
