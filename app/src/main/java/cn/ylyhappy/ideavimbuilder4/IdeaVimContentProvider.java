package cn.ylyhappy.ideavimbuilder4;

import static cn.ylyhappy.ideavimbuilder4.db.DbHelper.ACTION_ID;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.ylyhappy.ideavimbuilder4.db.DbHelper;

/*
ContentProvider，
它负责提供的数据表就是Action
* */
public class IdeaVimContentProvider extends ContentProvider {

    private static final String INNER_URI = "cn.ylyhappy.ideavimbuilder4.hello";

    public static final Uri CONTENT_URI = Uri.parse("content://"+ INNER_URI +"/elements");
    private static final int ALLROWS = 1;
    private static final int SINGLE_ROW = 2;
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(INNER_URI, "element", ALLROWS);
        uriMatcher.addURI(INNER_URI, "elements/#", SINGLE_ROW);
    }


    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        switch (uriMatcher.match(uri)){
            case SINGLE_ROW: {
                String rowId = uri.getPathSegments().get(1);
                String query = DbHelper.getInstance().getIdeaVimActionByIdQueryString(Integer.valueOf(rowId));
                return DbHelper.getInstance().getDatabase().rawQuery(query,null);
            }
        }

        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){
            case ALLROWS:
                return "vnd.android.cursor.dir/vnd.paad.elemental";
            case SINGLE_ROW:
                return "vnd.android.cursor.item/vnd.paad.elemental";
        }
        return "";
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = DbHelper.getInstance().getDatabase();
        switch (uriMatcher.match(uri)){
            case SINGLE_ROW:{
                String rowId = uri.getPathSegments().get(1);
                selection = ACTION_ID + " = " + rowId + (!TextUtils.isEmpty(selection) ? "AND ("
                        + selection + ")" : "");
                Toast.makeText(this.getContext(),"delte context 88", Toast.LENGTH_SHORT).show();
            }
        }

        if (selection == null) selection = "1";
        int deleteCount = db.delete("Action", selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);

        return deleteCount;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
