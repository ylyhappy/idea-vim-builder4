/*
 * Copyright (C) 2013-2024 Federico Iosue (federico@iosue.it)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cn.ylyhappy.ideavimbuilder4;


import static cn.ylyhappy.ideavimbuilder4.constant.Constant.LOG_TAG;

import android.util.Log;

import cn.ylyhappy.ideavimbuilder4.exceptions.GenericException;

public class LogDelegate {

    private static Boolean fileLoggingEnabled;

    public static void v(String message) {
        if (isFileLoggingEnabled()) {

        } else {
            Log.v(LOG_TAG, message);
        }
    }

    public static void d(String message) {
        if (isFileLoggingEnabled()) {

        } else {
            Log.d(LOG_TAG, message);
        }
    }

    public static void i(String message) {
        if (isFileLoggingEnabled()) {

        } else {
            Log.i(LOG_TAG, message);
        }
    }

    public static void w(String message, Throwable e) {
        if (isFileLoggingEnabled()) {

        } else {
            Log.w(LOG_TAG, message, e);
        }
    }

    public static void w(String message) {
        if (isFileLoggingEnabled()) {

        } else {
            Log.w(LOG_TAG, message);
        }
    }

    public static void e(String message, Throwable e) {
        if (isFileLoggingEnabled()) {

        } else {
            Log.e(LOG_TAG, message, e);
        }
    }

    public static void e(String message) {
        e(message, new GenericException(message));
    }

    private static boolean isFileLoggingEnabled() {
        if (fileLoggingEnabled == null) {
        }
        return false;
    }
}
