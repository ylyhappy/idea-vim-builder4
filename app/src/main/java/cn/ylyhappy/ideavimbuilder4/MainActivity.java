package cn.ylyhappy.ideavimbuilder4;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import cn.ylyhappy.ideavimbuilder4.constant.Constant;
import cn.ylyhappy.ideavimbuilder4.db.DbHelper;
import cn.ylyhappy.ideavimbuilder4.model.IdeaVimConfig;
import cn.ylyhappy.ideavimbuilder4.recyclerlist.MainActivityRecyclerListAdapter;

public class MainActivity extends BaseActivity {

    private Handler handler;
    private final int FINISH_LOAD = 0x100001;
    private IdeaVimConfig curConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        initInsets();

        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == FINISH_LOAD) {
                    findViewById(R.id.activity_main_context).setVisibility(View.VISIBLE);
                    findViewById(R.id.activity_main_progressbar1).setVisibility(View.GONE);
                }
            }
        };

        // init recycler view list
        RecyclerView recyclerView = findViewById(R.id.activity_main_recyclerview);
        ArrayList<IdeaVimConfig> list = (ArrayList<IdeaVimConfig>) DbHelper.getInstance().getIdeaVimConfigs();
        MainActivityRecyclerListAdapter adapter = new MainActivityRecyclerListAdapter(list, this, e -> curConfig = e);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // bind drawerlayout menu onclick

        NavigationView navigationView = findViewById(R.id.activity_main_navigationview);
        navigationView.getMenu().findItem(R.id.activity_main_menu_item1)
                .setOnMenuItemClickListener(e -> {
                    Intent intent = new Intent(MainActivity.this, ActionListActivity.class);
                    intent.putExtra(Constant.INTENT_IDEA_VIM_CONFIG_DATA, curConfig);
                    startActivity(intent);
                    return false;
                });
        navigationView.getMenu().findItem(R.id.activity_main_menu_item2)
                .setOnMenuItemClickListener(e -> {
                    Intent intent = new Intent(MainActivity.this, ConfigPreviewActivity.class);
                    intent.putExtra(Constant.INTENT_IDEA_VIM_CONFIG_DATA, curConfig);
                    startActivity(intent);
                    return false;
                });

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                DbHelper dbHelper = DbHelper.getInstance();
                dbHelper.getDatabase(true);
                Message message = new Message();
                message.what = FINISH_LOAD;
                handler.sendMessage(message);
            }
        });
        t1.start();

    }
}
