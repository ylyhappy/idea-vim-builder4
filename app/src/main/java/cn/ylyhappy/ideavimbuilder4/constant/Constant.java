package cn.ylyhappy.ideavimbuilder4.constant;

public class Constant {
    static final public String LOG_TAG = "IDEAVIM";

    // intent transport data define
    static final public String INTENT_IDEA_VIM_CONFIG_DATA= "intent-idea-vim-config-data";
}
