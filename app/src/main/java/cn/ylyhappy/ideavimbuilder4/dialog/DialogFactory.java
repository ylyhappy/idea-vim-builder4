package cn.ylyhappy.ideavimbuilder4.dialog;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.textfield.TextInputEditText;

import cn.ylyhappy.ideavimbuilder4.ActionListActivity;
import cn.ylyhappy.ideavimbuilder4.R;
import cn.ylyhappy.ideavimbuilder4.db.DbHelper;
import cn.ylyhappy.ideavimbuilder4.model.IdeaVimAction;
import cn.ylyhappy.ideavimbuilder4.model.LineDTO;
import cn.ylyhappy.ideavimbuilder4.model.PostActionItemCheckedChangerHandler;


public class DialogFactory {

    static public AlertDialog buildActionActivityModalFormDialog(ActionListActivity ac,long cfgId, IdeaVimAction e, PostActionItemCheckedChangerHandler p) {
        // 创建对话框的布局
        LayoutInflater inflater = ac.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_action_list_form, null);

        // 创建对话框
        AlertDialog.Builder builder = new AlertDialog.Builder(ac);
        ((TextInputEditText)(dialogView.findViewById(R.id.line_action_en_text    ))).setText(e.getEn());
        ((TextInputEditText)(dialogView.findViewById(R.id.line_action_zh_text    ))).setText(e.getZh());

        builder.setTitle("Editor IdeaVim Single Line")
                .setView(dialogView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // OK
                        String map          = ((TextInputEditText)(dialogView.findViewById(R.id.line_map_text          ))).getText().toString();
                        String aPre         = ((TextInputEditText)(dialogView.findViewById(R.id.line_action_pre_text   ))).getText().toString();
                        String aAfter       = ((TextInputEditText)(dialogView.findViewById(R.id.line_action_after_text ))).getText().toString();
                        String aShortcut    = ((TextInputEditText)(dialogView.findViewById(R.id.line_shortcut_text     ))).getText().toString();
                        String cr           = ((TextInputEditText)(dialogView.findViewById(R.id.line_cr_text           ))).getText().toString();
                        LineDTO lineDTO = new LineDTO();
                        lineDTO.setMap(map).setActionPre(aPre)
                               .setConfigId(1)
                               .setActionId(e.getId()).setActionaft(aAfter)
                               .setShortcut(aShortcut).setCr(cr);
                        DbHelper.getInstance().insertLine(lineDTO);
                        p.post();
                        Toast.makeText(ac,"添加数据成功", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // 显示对话框
        AlertDialog dialog = builder.create();
        return dialog;
    }
}
