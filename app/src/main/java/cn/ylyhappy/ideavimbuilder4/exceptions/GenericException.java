package cn.ylyhappy.ideavimbuilder4.exceptions;

public class GenericException extends RuntimeException{
    public GenericException(String message){
        super(message);

    }
}
