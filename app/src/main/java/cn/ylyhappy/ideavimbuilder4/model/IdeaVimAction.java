

package cn.ylyhappy.ideavimbuilder4.model;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

public class IdeaVimAction implements Parcelable {
    private int id;
    private String en;
    private String zh;
    private String tag;
    private Boolean selected;


    public IdeaVimAction(){}
    public IdeaVimAction(int id){
    	this.setId(id);
    }
    public IdeaVimAction(int id, String en){
    	this.setId(id);
    	this.setEn(en);
    }
    public IdeaVimAction(String en, String zh, Boolean selected){
    	this.setEn(en);
    	this.setZh(zh);
        this.selected = selected;
    }
    public IdeaVimAction(int id, String en, String zh, String tag){
    	this.setId(id);
    	this.setEn(en);
    	this.setZh(zh);
    	this.setTag(tag);
    }
    

    protected IdeaVimAction(Parcel in) {
        this.id = in.readInt();
        this.en = in.readString();
        this.zh = in.readString();
        this.tag = in.readString();
        
    }

    public int getId () {
    	return id;
    }
    public IdeaVimAction setId(int id) {
    	this.id = id;
    	return this;
    }
    public String getEn () {
    	return en;
    }
    public IdeaVimAction setEn(String en) {
    	this.en = en;
    	return this;
    }
    public String getZh () {
    	return zh;
    }
    public IdeaVimAction setZh(String zh) {
    	this.zh = zh;
    	return this;
    }
    public String getTag () {
    	return tag;
    }
    public IdeaVimAction setTag(String tag) {
    	this.tag = tag;
    	return this;
    }
    public Boolean getSelected() {
        return selected;
    }

    public IdeaVimAction setSelected(Boolean selected) {
        this.selected = selected;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel out, int i) {
        out.writeInt(this.id);
        out.writeString(this.en);
        out.writeString(this.zh);
        out.writeString(this.tag);
        
    }

    public static final Creator<IdeaVimAction> CREATOR = new Creator<IdeaVimAction>() {
        @Override
        public IdeaVimAction createFromParcel(Parcel source) {
            return new IdeaVimAction(source);
        }

        @Override
        public IdeaVimAction[] newArray(int size) {
            return new IdeaVimAction[size];
        }
    };
}
