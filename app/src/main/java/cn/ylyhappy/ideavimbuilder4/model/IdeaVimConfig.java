package cn.ylyhappy.ideavimbuilder4.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class IdeaVimConfig implements Parcelable {
    private int id;
    private String name;
    private Boolean selected;
    private String comment;

    public IdeaVimConfig() {
    }

    public IdeaVimConfig(String name, Boolean selected) {
        this.name = name;
        this.selected = selected;
    }

    protected IdeaVimConfig(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.selected = in.readBoolean();
        this.comment = in.readString();
    }

    public int getId() {
        return id;
    }

    public IdeaVimConfig setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public IdeaVimConfig setName(String name) {
        this.name = name;
        return this;
    }

    public Boolean getSelected() {
        return selected;
    }

    public String getComment() {
        return comment;
    }

    public IdeaVimConfig setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public IdeaVimConfig setSelected(Boolean selected) {
        this.selected = selected;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.name);
        parcel.writeBoolean(this.selected);
        parcel.writeString(this.comment);
    }

    public static final Parcelable.Creator<IdeaVimConfig> CREATOR = new Parcelable.Creator<IdeaVimConfig>() {
        @Override
        public IdeaVimConfig createFromParcel(Parcel source) {
            return new IdeaVimConfig(source);
        }

        @Override
        public IdeaVimConfig[] newArray(int size) {
            return new IdeaVimConfig[size];
        }
    };
}
