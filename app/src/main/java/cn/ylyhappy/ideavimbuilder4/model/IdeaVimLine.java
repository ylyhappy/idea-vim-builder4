

package cn.ylyhappy.ideavimbuilder4.model;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

public class IdeaVimLine implements Parcelable {
    private int id;
    private int action_id;
    private String map;
    private String actionpre;
    private String shortcut;
    private String actionafter;
    private String cr;
    private String source;
    

    public IdeaVimLine(){}
    public IdeaVimLine(int id){
    	this.setId(id);
    }
    public IdeaVimLine(int id, int action_id){
    	this.setId(id);
    	this.setAction_id(action_id);
    }
    public IdeaVimLine(int id, int action_id, String map){
    	this.setId(id);
    	this.setAction_id(action_id);
    	this.setMap(map);
    }
    public IdeaVimLine(int id, int action_id, String map, String actionpre){
    	this.setId(id);
    	this.setAction_id(action_id);
    	this.setMap(map);
    	this.setActionpre(actionpre);
    }
    public IdeaVimLine(int id, int action_id, String map, String actionpre, String shortcut){
    	this.setId(id);
    	this.setAction_id(action_id);
    	this.setMap(map);
    	this.setActionpre(actionpre);
    	this.setShortcut(shortcut);
    }
    public IdeaVimLine(int id, int action_id, String map, String actionpre, String shortcut, String actionafter){
    	this.setId(id);
    	this.setAction_id(action_id);
    	this.setMap(map);
    	this.setActionpre(actionpre);
    	this.setShortcut(shortcut);
    	this.setActionafter(actionafter);
    }
    public IdeaVimLine(int id, int action_id, String map, String actionpre, String shortcut, String actionafter, String cr){
    	this.setId(id);
    	this.setAction_id(action_id);
    	this.setMap(map);
    	this.setActionpre(actionpre);
    	this.setShortcut(shortcut);
    	this.setActionafter(actionafter);
    	this.setCr(cr);
    }
    public IdeaVimLine(int id, int action_id, String map, String actionpre, String shortcut, String actionafter, String cr, String source){
    	this.setId(id);
    	this.setAction_id(action_id);
    	this.setMap(map);
    	this.setActionpre(actionpre);
    	this.setShortcut(shortcut);
    	this.setActionafter(actionafter);
    	this.setCr(cr);
    	this.setSource(source);
    }
    

    protected IdeaVimLine(Parcel in) {
        this.id = in.readInt();
        this.action_id = in.readInt();
        this.map = in.readString();
        this.actionpre = in.readString();
        this.shortcut = in.readString();
        this.actionafter = in.readString();
        this.cr = in.readString();
        this.source = in.readString();
        
    }

    public int getId () {
    	return id;
    }
    public IdeaVimLine setId(int id) {
    	this.id = id;
    	return this;
    }
    public int getAction_id () {
    	return action_id;
    }
    public IdeaVimLine setAction_id(int action_id) {
    	this.action_id = action_id;
    	return this;
    }
    public String getMap () {
    	return map;
    }
    public IdeaVimLine setMap(String map) {
    	this.map = map;
    	return this;
    }
    public String getActionpre () {
    	return actionpre;
    }
    public IdeaVimLine setActionpre(String actionpre) {
    	this.actionpre = actionpre;
    	return this;
    }
    public String getShortcut () {
    	return shortcut;
    }
    public IdeaVimLine setShortcut(String shortcut) {
    	this.shortcut = shortcut;
    	return this;
    }
    public String getActionafter () {
    	return actionafter;
    }
    public IdeaVimLine setActionafter(String actionafter) {
    	this.actionafter = actionafter;
    	return this;
    }
    public String getCr () {
    	return cr;
    }
    public IdeaVimLine setCr(String cr) {
    	this.cr = cr;
    	return this;
    }
    public String getSource () {
    	return source;
    }
    public IdeaVimLine setSource(String source) {
    	this.source = source;
    	return this;
    }
    


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel out, int i) {
        out.writeInt(this.id);
        out.writeInt(this.action_id);
        out.writeString(this.map);
        out.writeString(this.actionpre);
        out.writeString(this.shortcut);
        out.writeString(this.actionafter);
        out.writeString(this.cr);
        out.writeString(this.source);
        
    }

    public static final Parcelable.Creator<IdeaVimLine> CREATOR = new Parcelable.Creator<IdeaVimLine>() {
        @Override
        public IdeaVimLine createFromParcel(Parcel source) {
            return new IdeaVimLine(source);
        }

        @Override
        public IdeaVimLine[] newArray(int size) {
            return new IdeaVimLine[size];
        }
    };
}