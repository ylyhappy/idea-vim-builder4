package cn.ylyhappy.ideavimbuilder4.model;

import cn.ylyhappy.ideavimbuilder4.db.DbHelper;

/**
 * {@link DbHelper#insertLine()}
 */
public class LineDTO {
    private long configId;
    private long actionId;
    private String map;
    private String actionPre;
    private String shortcut;
    private String actionaft;
    private String cr;
    private String source;

    public long getConfigId() {
        return configId;
    }

    public LineDTO setConfigId(long configId) {
        this.configId = configId;
        return this;
    }

    public long getActionId() {
        return actionId;
    }

    public LineDTO setActionId(long actionId) {
        this.actionId = actionId;
        return this;
    }

    public String getMap() {
        return map;
    }

    public LineDTO setMap(String map) {
        this.map = map;
        return this;
    }

    public String getActionPre() {
        return actionPre;
    }

    public LineDTO setActionPre(String actionPre) {
        this.actionPre = actionPre;
        return this;
    }

    public String getShortcut() {
        return shortcut;
    }

    public LineDTO setShortcut(String shortcut) {
        this.shortcut = shortcut;
        return this;
    }

    public String getActionaft() {
        return actionaft;
    }

    public LineDTO setActionaft(String actionaft) {
        this.actionaft = actionaft;
        return this;
    }

    public String getCr() {
        return cr;
    }

    public LineDTO setCr(String cr) {
        this.cr = cr;
        return this;
    }

    public String getSource() {
        return source;
    }

    public LineDTO setSource(String source) {
        this.source = source;
        return this;
    }
}
