package cn.ylyhappy.ideavimbuilder4.model;

public interface OnActionItemCheckedChangerListener {
    void onCheckedChanged(long cfgId,IdeaVimAction ideaVimAction, PostActionItemCheckedChangerHandler post);
}

