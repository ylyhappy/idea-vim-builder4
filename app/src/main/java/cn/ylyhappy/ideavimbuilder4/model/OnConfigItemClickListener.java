package cn.ylyhappy.ideavimbuilder4.model;

public interface OnConfigItemClickListener {
    void onConfigItemClick(IdeaVimConfig ideaVimConfig);
}
