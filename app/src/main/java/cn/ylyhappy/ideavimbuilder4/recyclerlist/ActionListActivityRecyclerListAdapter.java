package cn.ylyhappy.ideavimbuilder4.recyclerlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import cn.ylyhappy.ideavimbuilder4.LogDelegate;
import cn.ylyhappy.ideavimbuilder4.R;
import cn.ylyhappy.ideavimbuilder4.db.DbHelper;
import cn.ylyhappy.ideavimbuilder4.model.IdeaVimAction;
import cn.ylyhappy.ideavimbuilder4.model.OnActionItemCheckedChangerListener;
import cn.ylyhappy.ideavimbuilder4.model.PostActionItemCheckedChangerHandler;

public class ActionListActivityRecyclerListAdapter extends RecyclerView.Adapter<ActionListActivityRecyclerListHolder> {

    private ArrayList<IdeaVimAction> list;
    // back list, because need filter
    private ArrayList<IdeaVimAction> source_list;
    private final OnActionItemCheckedChangerListener onActionItemCheckedChangerListener;
    private int configId;
    private final HashMap<Integer, Boolean> cache = new HashMap<>(500);
    private Boolean refreshCache = true;

    public ActionListActivityRecyclerListAdapter(ArrayList<IdeaVimAction> list, OnActionItemCheckedChangerListener onActionItemCheckedChangerListener) {
        this.source_list = list;
        this.list = list;
        this.onActionItemCheckedChangerListener = onActionItemCheckedChangerListener;
    }

    @NonNull
    @Override
    public ActionListActivityRecyclerListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_action_list_item, parent, false);
        ActionListActivityRecyclerListHolder holder = new ActionListActivityRecyclerListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ActionListActivityRecyclerListHolder holder, int position) {
        if (refreshCache){
            DbHelper.getInstance().getActionIdsByConfigId(configId).forEach(e -> {
                cache.put(e, true);
            });
            refreshCache = false;
        }
        IdeaVimAction ideaVimAction = this.list.get(position);
        holder.en.setText(ideaVimAction.getEn());
        holder.zh.setText(ideaVimAction.getZh());
        holder.cb.setChecked(ideaVimAction.getSelected());
        holder.cb.setOnClickListener(e -> {
            this.onActionItemCheckedChangerListener.onCheckedChanged(configId, ideaVimAction, () -> updateItem(position, ideaVimAction));
        });
        holder.cb.setChecked(cache.get(ideaVimAction.getId()) != null);
    }

    private void setActionItemSeleted(int pos,boolean v){
        this.source_list.get(pos).setSelected(v);
    }

    private void refreshCache(){
        this.cache.clear();
        DbHelper.getInstance().getActionIdsByConfigId(configId).forEach(e -> {
            cache.put(e, true);
        });
    }


    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public void setFilter(Predicate<? super IdeaVimAction> predicate){
        this.list = (ArrayList<IdeaVimAction>) this.source_list.stream().filter(predicate).collect(Collectors.toList());
        notifyDataSetChanged();
    }

    public void setConifgId(int configId){
        this.configId = configId;
    }

    public void updateItem(int pos, IdeaVimAction e){
        LogDelegate.v("UPDATE ITEM, IDEAVIMACTION");
        refreshCache();
        notifyItemChanged(pos);
    }

}
