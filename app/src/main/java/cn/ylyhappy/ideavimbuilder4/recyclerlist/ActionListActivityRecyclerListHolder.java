package cn.ylyhappy.ideavimbuilder4.recyclerlist;


import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import cn.ylyhappy.ideavimbuilder4.R;

public class ActionListActivityRecyclerListHolder extends RecyclerView.ViewHolder {
    TextView en;
    TextView zh;
    CheckBox cb;

    public ActionListActivityRecyclerListHolder(@NonNull View itemView) {
        super(itemView);
        en = itemView.findViewById(R.id.activity_action_list_item_textview1);
        zh = itemView.findViewById(R.id.activity_action_list_item_textview2);
        cb = itemView.findViewById(R.id.activity_action_list_item_checkbox1);
    }
}
