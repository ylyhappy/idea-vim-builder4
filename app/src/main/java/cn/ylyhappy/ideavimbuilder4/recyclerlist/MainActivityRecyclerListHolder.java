package cn.ylyhappy.ideavimbuilder4.recyclerlist;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import cn.ylyhappy.ideavimbuilder4.R;

public class MainActivityRecyclerListHolder extends RecyclerView.ViewHolder {

    private TextView tv1;
    private CheckBox cb1;

    public MainActivityRecyclerListHolder(@NonNull View itemView) {
        super(itemView);
        this.tv1 = itemView.findViewById(R.id.activity_main_expand_tv1);
        this.cb1 = itemView.findViewById(R.id.activity_main_expand_cb1);
    }

    public TextView getTv1() {
        return tv1;
    }

    public MainActivityRecyclerListHolder setTv1(TextView tv1) {
        this.tv1 = tv1;
        return this;
    }

    public CheckBox getCb1() {
        return cb1;
    }

    public MainActivityRecyclerListHolder setCb1(CheckBox cb1) {
        this.cb1 = cb1;
        return this;
    }

}
